/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.application;

import java.util.Properties;
import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

/**
 * Classe principal do sistema. Configura a inicialização do contexto do
 * SpringBoot e define o modo de operação dos logs.
 *
 * @author anderson
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.anderson")
public class Application {

    public static void main(String[] args) {

        /**
         * Inicializa o SpringBoot e todo o contexto do aplicativo. Seta o nível
         * de log base para ERROR para evitar sujeira de inicialização do
         * SpringBoor nos logs.
         */
        Properties properties = new Properties();
        properties.put("logging.level.root", "ERROR");

        new SpringApplicationBuilder()
                .properties(properties)
                .bannerMode(Banner.Mode.OFF)
                .sources(Application.class)
                .run(args);

    }
}
