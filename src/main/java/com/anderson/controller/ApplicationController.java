/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.controller;

import com.anderson.util.Util;
import static com.anderson.util.Util.LOGGER;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author anderson
 */
@Component
public class ApplicationController {

    @Autowired
    private DatFileReaderController datFileReaderController;

    @PostConstruct
    void configureApplication() {
        
        Configurator.setLevel(LOGGER.getName(), Level.getLevel("DEBUG"));
        LOGGER.info("Inicializando sistema.");

        Util.DATA_FOLDER_INIT();

        /**
         * Inicializa o WatchFile
         */
        datFileReaderController.initWatchFile();

    }

    @PreDestroy
    protected void destroyApplication() {

        System.out.println("Encerrando o sistema!");

    }
}
