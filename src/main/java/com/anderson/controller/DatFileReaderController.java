/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.controller;

import com.anderson.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author anderson
 */
@Component
public class DatFileReaderController {

    @Autowired
    private WatchFileController watchFileController;

    /**
     * Intancia a classe WatchFileController passando o caminho da pasta dos
     * vídeos para processar os arquivos novos que forem transferidos por ftp.
     * Executa o processEvents somente tratando o evento de criação de arquivo
     * que não contenha o nome terminado com _temp.
     */
    public void initWatchFile() {

        watchFileController.setDir(Util.IN_DATA);
        watchFileController.init();
        watchFileController.processEvents();

    }
}
