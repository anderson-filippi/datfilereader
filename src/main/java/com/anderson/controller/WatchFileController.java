package com.anderson.controller;

import com.anderson.service.DatFileReaderService;
import static com.anderson.util.Util.LOGGER;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author anderson
 */
@Component
public class WatchFileController {

    @Autowired
    private DatFileReaderService datFileReaderService;

    private WatchService watcher = null;
    private Map<WatchKey, Path> keys = null;

    private Path dir;

    public WatchFileController() {
    }

    public void init() {
        try {
            this.watcher = FileSystems.getDefault().newWatchService();
            this.keys = new HashMap<>();

            registerDirectory(dir);
            LOGGER.info("Registrando {} no monitoramento de arquivos dat.", dir);
        } catch (IOException ex) {
            LOGGER.catching(ex);
        }

    }

    /**
     * Registra somente o evento create file para um diretório
     *
     * @param dir
     * @throws IOException
     */
    private void registerDirectory(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE);
        keys.put(key, dir);
    }

    /**
     * Processa os eventos
     *
     */
    public void processEvents() {
        /**
         * Define a extensão de arquivo válida.
         */
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.dat");

        /**
         * Aguarda a sinalização de uma chave
         */
        for (;;) {

            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException e) {
                return;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                LOGGER.error("Chave não reconhecida!");
                continue;
            }

            for (WatchEvent<?> event : key.pollEvents()) {

                @SuppressWarnings("rawtypes")
                WatchEvent.Kind kind = event.kind();

                /**
                 * Contexto para evento de diretorio é o nome do arquivo da
                 * entrada
                 */
                @SuppressWarnings("unchecked")
                Path name = ((WatchEvent<Path>) event).context();
                Path child = dir.resolve(name);

                /**
                 * Avalia somente a criação de arquivos com a extensão válida.
                 */
                if ((kind == ENTRY_CREATE) && matcher.matches(child.getFileName())) {

                    LOGGER.info("Arquivo {} criado.", child);

                    try {
                        datFileReaderService.readCsvFile(child);
                    } catch (Exception ex) {
                        LOGGER.catching(ex);
                    }
                }
            }

            /**
             * Reseta o diretorio e remove do map se o diretorio não está mais
             * acessivel
             */
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
    }

    public Path getDir() {
        return dir;
    }

    public void setDir(Path dir) {
        this.dir = dir;
    }
}
