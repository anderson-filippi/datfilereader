/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.model;

/**
 *
 * @author anderson
 */
public class Cliente {

    private String CNPJ;
    private String nome;
    private String ramoAtividade;

    public Cliente() {
    }

    public Cliente(String CNPJ, String nome, String ramoAtividade) {
        this.CNPJ = CNPJ;
        this.nome = nome;
        this.ramoAtividade = ramoAtividade;
    }

    public String getCNPJ() {
        return CNPJ;
    }

    public void setCNPJ(String CNPJ) {
        this.CNPJ = CNPJ;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRamoAtividade() {
        return ramoAtividade;
    }

    public void setRamoAtividade(String ramoAtividade) {
        this.ramoAtividade = ramoAtividade;
    }

}
