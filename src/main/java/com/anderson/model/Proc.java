/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.model;

import java.util.List;

/**
 *
 * @author anderson
 */
public class Proc {

    private int qtdClientes;
    private int qtdVendedores;
    private List<Venda> topRankedVendaList;
    private List<Vendedor> bottomRankedVendedorList;

    public Proc() {
    }

    public Proc(int qtdClientes, int qtdVendedores, List<Venda> topRankedVendaList, List<Vendedor> bottomRankedVendedorList) {
        this.qtdClientes = qtdClientes;
        this.qtdVendedores = qtdVendedores;
        this.topRankedVendaList = topRankedVendaList;
        this.bottomRankedVendedorList = bottomRankedVendedorList;
    }

    public int getQtdClientes() {
        return qtdClientes;
    }

    public void setQtdClientes(int qtdClientes) {
        this.qtdClientes = qtdClientes;
    }

    public int getQtdVendedores() {
        return qtdVendedores;
    }

    public void setQtdVendedores(int qtdVendedores) {
        this.qtdVendedores = qtdVendedores;
    }

    public List<Venda> getTopRankedVendaList() {
        return topRankedVendaList;
    }

    public void setTopRankedVendaList(List<Venda> topRankedVendaList) {
        this.topRankedVendaList = topRankedVendaList;
    }

    public List<Vendedor> getBottomRankedVendedorList() {
        return bottomRankedVendedorList;
    }

    public void setBottomRankedVendedorList(List<Vendedor> bottomRankedVendedorList) {
        this.bottomRankedVendedorList = bottomRankedVendedorList;
    }

}
