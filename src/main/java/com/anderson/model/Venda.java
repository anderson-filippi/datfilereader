/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.model;

/**
 *
 * @author anderson
 */
public class Venda {

    private long idVenda;
    private long idItem;
    private int qtd;
    private double preco;
    private double total;
    private Vendedor vendedor;

    public Venda() {
    }

    public Venda(long idVenda, long idItem, int qtd, double preco,double total, Vendedor vendedor) {
        this.idVenda = idVenda;
        this.idItem = idItem;
        this.qtd = qtd;
        this.preco = preco;
        this.total = total;
        this.vendedor = vendedor;
    }

    public long getIdVenda() {
        return idVenda;
    }

    public void setIdVenda(long idVenda) {
        this.idVenda = idVenda;
    }

    public long getIdItem() {
        return idItem;
    }

    public void setIdItem(long idItem) {
        this.idItem = idItem;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}
