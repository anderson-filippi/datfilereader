/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anderson.service;

import com.anderson.model.Cliente;
import com.anderson.model.Proc;
import com.anderson.model.Venda;
import com.anderson.model.Vendedor;
import com.anderson.util.Util;
import static com.anderson.util.Util.LOGGER;
import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import java.io.FileWriter;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

/**
 *
 * @author anderson
 */
@Service
public class DatFileReaderService {

    /**
     * Vendedores.
     */
    private List<Vendedor> vendedorList = new ArrayList<>();
    /**
     * Menor venda vendedor
     */
    private List<Vendedor> vendedorMenorVendaList = new ArrayList<>();
    /**
     * Clientes.
     */
    private List<Cliente> clienteList = new ArrayList<>();

    /**
     * Ranking vendas por vendedor
     */
    private Map<Vendedor, Double> topVendedorVendaMap = new HashMap<>();

    /**
     * Lista de vendas
     */
    private List<Venda> vendaList = new ArrayList<>();

    private void processData(Reader reader, Path path) throws Exception {
        clearLists();
        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(0)
                .withCSVParser(parser)
                .build();
        List<String[]> list = new ArrayList<>();
        list = csvReader.readAll();
        reader.close();
        csvReader.close();

        //percorre os itens da lista de registros
        for (String[] item : list) {
            switch (item[0]) {
                case "001": {
                    LOGGER.debug("Item " + item[0] + "-" + Util.DataType.VENDEDOR.name());
                    fillData(item, Util.DataType.VENDEDOR);
                    break;
                }
                case "002": {
                    LOGGER.debug("Item " + item[0] + "-" + Util.DataType.CLIENTE.name());
                    fillData(item, Util.DataType.CLIENTE);
                    break;
                }
                case "003": {
                    LOGGER.debug("Item " + item[0] + "-" + Util.DataType.VENDA.name());
                    fillData(item, Util.DataType.VENDA);
                    break;
                }
                default: {
                    throw new Exception("Arquivo " + path.getFileName().toString() + " contém registros inválidos ou fora do layout, cancelando processo!");
                }
            }

        }

        //quantidade de clientes:
        StringBuilder builder = new StringBuilder();
        builder.append("Arquivo: ");
        builder.append(path.getFileName());
        builder.append(" Quantidade de clientes: ");
        builder.append(clienteList.size());
        //quantidade de vendedores:
        builder.append(" Quantidade de vendedores: ");
        builder.append(vendedorList.size());

        //obter a venda de maior valor na lista, incluir em topVendaList e remover esta venda da lista de vendas
        List<Venda> topVendaList = new ArrayList<>();
        topVendaList.clear();
        Venda vendaTop = new Venda();
        for (Venda venda : vendaList) {
            //teste se o valor da venda
            if (venda.getTotal() > vendaTop.getTotal()) {
                vendaTop = venda;
            }
        }
        //inclui a primeira venda top
        topVendaList.add(vendaTop);

        //remove a venda top da lista
        for (Iterator it = vendaList.iterator(); it.hasNext();) {
            Venda venda = (Venda) it.next();
            if (venda.getIdVenda() == vendaTop.getIdVenda()) {
                it.remove();
                break;
            }
        }

        //ler a lista novamente e obter as vendas que possuirem o mesmo valor da maior/ultima encontrada
        //incluir os valores na lista
        for (Venda venda : vendaList) {
            if (venda.getTotal() == vendaTop.getTotal()) {
                topVendaList.add(venda);
            }
        }
        builder.append(" Id da venda(s) de valor mais alto: ");
        topVendaList.stream().forEach((Venda venda) -> {
            builder.append(venda.getIdVenda());
            builder.append(" ");
        });

        //Vendedor que vendeu menos
        Map.Entry<Vendedor, Double> min = null;
        for (Map.Entry<Vendedor, Double> entry : topVendedorVendaMap.entrySet()) {
            if (min == null || min.getValue() > entry.getValue()) {
                min = entry;
            }
        }
        Double minVenda = min.getValue();
        vendedorMenorVendaList.add(min.getKey());
        //remove o pior vendedor
        topVendedorVendaMap.remove(min.getKey());

        //busca outros vendedores que venderam o mesmo valor menor
        for (Map.Entry<Vendedor, Double> entry : topVendedorVendaMap.entrySet()) {
            if (minVenda == entry.getValue()) {
                vendedorMenorVendaList.add(min.getKey());
            }
        }
        builder.append(" Nome dos vendedor(es) que venderam menos: ");
        vendedorMenorVendaList.stream().forEach((Vendedor vendedor) -> {
            builder.append(vendedor.getNome());
            builder.append(" ");
        });

        LOGGER.info(builder);

        Proc proc = new Proc();
        proc.setTopRankedVendaList(topVendaList);
        proc.setBottomRankedVendedorList(vendedorMenorVendaList);
        proc.setQtdClientes(ObjectUtils.defaultIfNull(clienteList.size(), 0));
        proc.setQtdVendedores(ObjectUtils.defaultIfNull(vendedorList.size(), 0));

        csvWriterOneByOne(geraProc(proc), path);

//        return list;
    }

    public void readCsvFile(Path datpath) throws Exception {
        LOGGER.info("Lendo arquivo {}.", datpath.getFileName());
        Reader reader = Files.newBufferedReader(datpath);
        processData(reader, datpath);
    }

    private void fillData(String[] item, Util.DataType dataType) {
        try {
            switch (dataType) {
                case VENDEDOR: {
                    Double salario = Double.parseDouble(item[3]);
                    Vendedor vendedor = new Vendedor(item[1], item[2], salario);
                    vendedorList.add(vendedor);
                    break;
                }
                case CLIENTE: {
                    Cliente cliente = new Cliente(item[1], item[2], item[3]);
                    clienteList.add(cliente);
                    break;
                }
                case VENDA: {

                    //obtem o vendedor da lista de vendedores ex
                    Vendedor vendedor = new Vendedor();

                    for (Vendedor v : vendedorList) {
                        String nome = item[5];
                        if (v.getNome().equalsIgnoreCase(nome)) {
                            vendedor = v;
                            break;
                        }
                    }

                    Long idVenda = Long.parseLong(item[1]);
                    Long idItem = Long.parseLong(item[2]);
                    int qtdeItem = Integer.parseInt(item[3]);
                    Double precoItem = Double.parseDouble(item[4]);
                    Double valorTotal = Double.valueOf(String.valueOf(qtdeItem)) * precoItem;

                    Venda venda = new Venda(idVenda, idItem, qtdeItem, precoItem, valorTotal, vendedor);

                    //alimenta a lista de vendedor + soma das vendas
                    Double incValor = 0.0;
                    if (topVendedorVendaMap.containsKey(vendedor)) {
                        incValor = topVendedorVendaMap.get(vendedor) + valorTotal;
                        topVendedorVendaMap.replace(vendedor, incValor);
                    } else {
                        topVendedorVendaMap.put(vendedor, valorTotal);
                    }

                    /**
                     * top venda
                     */
//                    topVendaMap.put(venda.getTotal(), venda);
                    vendaList.add(venda);
                    break;
                }
            }

        } catch (NumberFormatException ex) {
            LOGGER.catching(ex);
        }
    }

    private List<String[]> geraProc(Proc proc) {
        List<String[]> stringArrList = new ArrayList<>();
        stringArrList.add(new String[]{"QtdClientes", String.valueOf(proc.getQtdClientes())});
        stringArrList.add(new String[]{"QtdVendedores", String.valueOf(proc.getQtdVendedores())});

        List<String> vendaStrList = new ArrayList<>();
        for (Venda venda : proc.getTopRankedVendaList()) {
            vendaStrList.add(String.valueOf(venda.getIdVenda()));
        }
        stringArrList.add(new String[]{"IdVendasValorMaisAlto", vendaStrList.toString()});

        List<String> vendedorStrList = new ArrayList<>();
        for (Vendedor vendedor : proc.getBottomRankedVendedorList()) {
            vendedorStrList.add(vendedor.getNome());
        }

        stringArrList.add(new String[]{"NomeVendedoresValorMaisBaixo", vendedorStrList.toString()});

        return stringArrList;
    }

    public void csvWriterOneByOne(List<String[]> stringArray, Path outPath) throws Exception {

        Path path = Util.OUT_DATA.resolve(outPath.getFileName() + ".out");

        CSVWriter writer = new CSVWriter(new FileWriter(path.toString()), ';', ' ', ' ', "\n");

        for (String[] array : stringArray) {

            writer.writeNext(array, false);
        }

        writer.close();
        LOGGER.info("Criou arquivo {}.", path);
    }

    private void clearLists() {
        /**
         * Vendedores.
         */
        vendedorList.clear();
        /**
         * Menor venda vendedor
         */
        vendedorMenorVendaList.clear();
        /**
         * Clientes.
         */
        clienteList.clear();
        /**
         * ID da Venda de valor mais alto: valor -> venda.
         */
//        topVendaMap.clear();

        topVendedorVendaMap.clear();
        /**
         * Lista de vendas
         */
        vendaList.clear();
    }
}
