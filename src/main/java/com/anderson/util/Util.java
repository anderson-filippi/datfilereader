package com.anderson.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author anderson
 */
public class Util {

    public static Logger LOGGER = LogManager.getLogger("logger_datfilereader");

    private static final Path BASE_PATH = Paths.get("./dados");
    public static Path IN_DATA = Paths.get(BASE_PATH.toString(), "in");
    public static Path OUT_DATA = Paths.get(BASE_PATH.toString(), "out");

    /**
     * Enum para classificar imagens de faces em: VALID, INVALID e MULTIPLE.
     */
    public static enum DataType {
        CLIENTE, VENDA, VENDEDOR;
    }

    public static void DATA_FOLDER_INIT() {

        LOGGER.debug("Pastas dados: {} {}", IN_DATA, OUT_DATA);

        if (Files.notExists(IN_DATA) || Files.notExists(OUT_DATA)) {
            try {
                Files.createDirectories(IN_DATA);
                Files.createDirectories(OUT_DATA);
                LOGGER.info("Pastas de dados criadas com sucesso!");
            } catch (IOException ex) {
                LOGGER.catching(ex);
            }
        }
    }
}
